<h1>about-me</h1>

## Something that I consider makes me different
I am a very creative and visual person, but with the ability to adapt myself to any more technical part, and still apply creativity.

## What is Git for me?
Is a version control that makes you work in a more efficient and organized way. Especially when it has to do with team projects in which more people work.

## What is Docker for me?
It is to create lightweight and portable containers for software applications that can run on any machine with Docker installed, regardless of the operating system that the machine has underneath, also facilitating deployments.

## What is testing for me?
Testing is a process in which the functionality of a program or application is verified and validated to ensure that the software product is free od defects.

## What JavaScript features do I know right now?
- Is sensitive to upper and lower case.
- Changes the content of a website based on events.
- Gets data from a server to display on a page.
- Makes statements in a loop.
- Makes decisions and perfoms actions based on a set of data, using conditionals.
- I know that works with data types, and they are: number, string, boolean, undefined, null, object and array.
- It is the basis for learning frameworks such as React, Vue or Angular.

## What Web Components are for me?
They are blocks of code that encapsulate the internal structure of HTML, making it easier to develop.
